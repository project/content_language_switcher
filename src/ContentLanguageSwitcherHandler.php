<?php

namespace Drupal\content_language_switcher;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for content translation handlers.
 *
 * @ingroup entity_api
 */
class ContentLanguageSwitcherHandler implements ContentLanguageSwitcherHandlerInterface, EntityHandlerInterface {

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The content translation manager service.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $manager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Initializes an instance of the content translation controller.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The info array of the given entity type.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $manager
   *   The content translation manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, LanguageManagerInterface $language_manager, ContentTranslationManagerInterface $manager, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, MessengerInterface $messenger, DateFormatterInterface $date_formatter) {
    $this->entityTypeId = $entity_type->id();
    $this->entityType = $entity_type;
    $this->languageManager = $language_manager;
    $this->manager = $manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('language_manager'),
      $container->get('content_translation.manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function entityFormAlter(array &$form, FormStateInterface $form_state, EntityInterface $entity) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    /** @var \Drupal\content_translation\ContentTranslationHandler $handler */

    if ($entity->isNew()) {
      return;
    }

    $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'translation');
    $languages = $this->languageManager->getLanguages();
    $original = $entity->getUntranslated()->language()->getId();
    $translations = $entity->getTranslationLanguages();
    $default_language = $this->languageManager->getDefaultLanguage();

    $form['content_language_switcher'] = [
      '#type' => 'container',
      '#title' => t('Language Switcher'),
      '#tree' => TRUE,
      '#access' => $handler->getTranslationAccess($entity, 'update')->isAllowed(),
      '#multilingual' => TRUE,
    ];

    $items = [];
    $current_language = NULL;

    foreach ($languages as $language) {
      $language_name = $language->getName();
      $langcode = $language->getId();
      $is_outdated = FALSE;
      $is_new = FALSE;

      $options = ['language' => $language];
      $add_url = $entity->toUrl('drupal:content-translation-add', $options)
        ->setRouteParameter('source', $original)
        ->setRouteParameter('target', $language->getId());
      $edit_url = $entity->toUrl('drupal:content-translation-edit', $options)
        ->setRouteParameter('language', $language->getId());
      $delete_url = $entity->toUrl('drupal:content-translation-delete', $options)
        ->setRouteParameter('language', $language->getId());

      $text = $language_name;
      if (array_key_exists($langcode, $translations)) {
        $translation = $entity->getTranslation($langcode);
        $metadata = $this->manager->getTranslationMetadata($translation);
        $is_outdated = $metadata->isOutdated();

        if ($default_language->getId() === $langcode) {
          $text .= ' (' . t('Default') . ')';
        }
        $url = $entity->toUrl('edit-form', ['language' => $language])->toString();
      }
      else {
        $url = $add_url->toString();
        $is_new = TRUE;
      }

      if ($entity->language()->getId() === $langcode) {
        $current_language = [
          'name' => $language_name,
          'code' => $langcode,
          'url' => $url,
          'is_outdated' => $is_outdated,
          'is_new' => $is_new,
        ];
      }
      else {
        $items[] = [
          'name' => $language_name,
          'code' => $langcode,
          'url' => $url,
          'is_outdated' => $is_outdated,
          'is_new' => $is_new,
        ];
      }
    }

    $weight = isset($form['langcode']) ? $form['langcode']['#weight'] : 0;
    $form['content_language_switcher']['#weight'] = $weight;

    $form['content_language_switcher']['switcher'] = [
      '#theme' => 'content_language_switcher',
      '#current_language' => $current_language,
      '#languages' => $items,
    ];

    if (isset($form['meta'])) {
      $form['content_language_switcher'] += [
        '#group' => 'meta',
        '#weight' => 100,
        '#attributes' => [
          'class' => ['entity-language-switcher-options'],
        ],
      ];
    }

  }

}
