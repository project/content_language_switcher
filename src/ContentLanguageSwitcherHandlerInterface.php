<?php

namespace Drupal\content_language_switcher;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for providing content language switcher.
 *
 * Defines a set of methods to allow any entity to be processed by the entity
 * translation UI.
 */
interface ContentLanguageSwitcherHandlerInterface {

  /**
   * Performs the needed alterations to the entity form.
   *
   * @param array $form
   *   The entity form to be altered to provide the translation workflow.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being created or edited.
   */
  public function entityFormAlter(array &$form, FormStateInterface $form_state, EntityInterface $entity);

}
